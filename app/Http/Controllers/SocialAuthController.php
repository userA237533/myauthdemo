<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    public function redirect_FB() {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback_FB(SocialAccountService $service) {
        // when facebook call us a with token
        //$providerUser = Socialite::driver('facebook')->user();
        //
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user()); 

        auth()->login($user) ;

        return redirect()->to('/home');
    }

    public function redirect_GITHUB() {
        return Socialite::driver('github')->redirect() ;
    }

    public function callback_GITHUB(SocialAccountService $service) {
        $user = $service->createOrGetUser_ByGITHUB(Socialite::driver('github')->user()) ;

        auth()->login($user) ;

        return redirect()->to('/home');
    }
}
