<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider('facebook')->whereProviderUserId($providerUser->getId())->first();

        if ($account) {
            return $account->user ;
        }else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]) ;

            //$user = User::whereEmail($providerUser->getEmail())->first() ;
            //$user = User::where('email', $providerUser->email)->first() ;
            $user = User::where('email', $providerUser->email)->first() ;

            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
            
        }
    }

    public function createOrGetUser_ByGITHUB(ProviderUser $providerUser) {
        $account = SocialAccount::where('Provider', 'github')->where('provider_user_id', $providerUser->getId())->first();

        if( $account ) {
            return $account->user ;
        }else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'github'
            ]) ;

            $user = User::where('email', $providerUser->email)->first() ;

            if(!$user) {
                
                //echo var_dump($providerUser) ;
                
                $user = User::create([
                    'email' => $providerUser->email,
                    'name' => $providerUser->nickname
                    //'password' => $providerUser->id
                ]) ;
            }
           //echo var_dump($user) ;
           //echo var_dump($account) ;

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}

?>