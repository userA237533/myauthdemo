<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

/**
 * FB login
 */
Route::get('/redirect_FB', 'SocialAuthController@redirect_FB');
Route::get('/callback_FB', 'SocialAuthController@callback_FB');

/**
 * GITHUB login
 */
Route::get('/redirect_GITHUB', 'SocialAuthController@redirect_GITHUB');
Route::get('/callback_GITHUB', 'SocialAuthController@callback_GITHUB');
